﻿<!DOCTYPE ="html">
<html language="en-us">
<head>
    <meta charset="utf-8">
    <meta name="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Marcus Cook" backend="">
    <meta name="keywords" content="">

    <title>Construction Management Home</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap-theme.css">
    <link rel="stylesheet" href="../Styles/modern-business.css">
    <link rel="stylesheet" href="../Styles/Styles.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/2.0.0-beta.17/angular2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-google-maps/2.3.3/angular-google-maps.min.js"></script>
    <style></style>
</head>
<body >
<header>
    <?php
	   include '../Elements/header.php';
    ?>
</header>
<main>
    <?php
	   include '../Pages/home.php';
    ?>
</main>    
<footer>
    <?php
	  include '../Elements/footer.php';
    ?>
</footer>
</body>
</html>
