﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public interface IAuthProvider
    {
        int Authenticate(string username, string password);
    }
}