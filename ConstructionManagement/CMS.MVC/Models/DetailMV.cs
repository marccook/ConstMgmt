﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class DetailMV
    {
        public int ID { get; set; }
        [Required]
        public int SiteID { get; set; }
        [Required]
        [RegularExpression("^[0-9]{3}-[0-9]{2}-[0-9]{4}$")]
        public string SSN { get; set; }
        [RegularExpression("^[0-9]{3}-[0-9]{3}-[0-9]{4}$")]
        public string phone { get; set; }
        public string username { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public System.DateTime DateHired { get; set; }
        [Required]
        public string password { get; set; }
        public Nullable<System.DateTime> DateModified { get; set; }
        public Nullable<bool> Active { get; set; }

    }
}
