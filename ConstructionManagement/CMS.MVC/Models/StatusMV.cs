﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CMS.MVC.Models
{
    public class StatusMV
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}