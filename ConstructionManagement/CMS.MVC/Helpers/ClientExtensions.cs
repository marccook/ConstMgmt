﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Cms.DomainLogic;
using CMS.MVC;
using CMS.MVC.Models;

namespace CMS.MVC.Helpers
{
    public static class ClientExtensions
    {
        public static List<EmployeeMV> ToViewModel(this List<PersonDAO> p)
        {
            var temp = new List<EmployeeMV>();
            foreach (var item in p)
            {
                temp.Add(item.ToClient());
            }
            return temp;
        }

        public static List<StatusMV> ToViewModel(this List<StatusDao> s)
        {
            var temp = new List<StatusMV>();
            foreach (var item in s)
            {
                temp.Add(item.ToClient());
            }
            return temp;
        }

        public static List<DetailMV> ToViewModel(this List<DetailsDao> d)
        {
            var temp = new List<DetailMV>();
            foreach (var item in d)
            {
                temp.Add(item.ToClient());

            }
            return temp;
        }

        public static List<ScheduleMV> ToViewModel(this List<ScheduleDAO> d)
        {
            var temp = new List<ScheduleMV>();
            foreach (var item in d)
            {
                temp.Add(item.ToClient());
            }
            return temp;
        }

        public static List<PersonTaskMV> ToViewModel(this List<PersonTaskDao> pt)
        {
            var temp = new List<PersonTaskMV>();
            foreach (var item in pt)
            {
                temp.Add(item.ToClient());
            }
            return temp;
        }

      
        
    }
}