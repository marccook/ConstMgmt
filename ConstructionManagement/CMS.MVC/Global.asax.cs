﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Cms.DomainLogic;

using CMS.MVC.Models;

namespace CMS.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
     
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
