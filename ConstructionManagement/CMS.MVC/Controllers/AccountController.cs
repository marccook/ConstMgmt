﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.MVC.Models;

namespace CMS.MVC.Controllers
{
    public class AccountController:Controller
    {
        IAuthProvider authProvider;

        public AccountController(IAuthProvider auth)
        {
            authProvider = auth;
        }

        public ViewResult Login()
        {
            return View();

        }

        public ActionResult Logout()
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
            return Redirect(Url.Action("Index", "Admin"));
        }
        [HttpPost]
        public ActionResult Login(LoginVM model, string returlUrl)
        {
            if (ModelState.IsValid)

            {
                try
                {
                    var role = authProvider.Authenticate(model.username, model.password);
                    if (role < 4 && role > 0)
                    {
                        ModelState.AddModelError("", "This user cannot access administrative options. Please contact your supervisor if this message is received in error.");
                        return View();

                    }
                    else if (role == 4)
                    {

                        return Redirect(returlUrl ?? Url.Action("Index", "Admin"));
                    }
                    else
                    {
                        ModelState.AddModelError("", "Incorrect username or password");
                        return View();
                    }
                }
                catch 
                {
                   
                    return View();
                }

            }
            else
            {
                return View();
            }
        }
    }

}
